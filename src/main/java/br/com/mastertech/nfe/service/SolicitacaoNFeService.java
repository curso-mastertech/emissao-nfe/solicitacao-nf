package br.com.mastertech.nfe.service;

import br.com.mastertech.nfe.exception.IdentidadeEmissaoNFeException;
import br.com.mastertech.nfe.model.LogNFe;
import br.com.mastertech.nfe.model.NFe;
import br.com.mastertech.nfe.model.SolicitacaoNFe;
import br.com.mastertech.nfe.repository.NFeRepository;
import br.com.mastertech.nfe.repository.SolicitacaoNFeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolicitacaoNFeService {

    public static final String TOPICO_EMISSAO = "pedro-biro-1";
    public static final String TOPICO_LOG = "pedro-biro-2";

    @Autowired
    private KafkaTemplate<String, SolicitacaoNFe> filaEmissao;
    @Autowired
    private KafkaTemplate<String, LogNFe> filaLog;

    @Autowired
    private SolicitacaoNFeRepository solicitacaoRepo;
    @Autowired
    private NFeRepository nfeRepo;

    public SolicitacaoNFe solicita(SolicitacaoNFe solicitacao) {
//        //valida identidade (cpf ou cnpj)
//        validaCPFouCNPJ(solicitacao.getIdentidade());
        //salva solicitacao
        SolicitacaoNFe solicitacaoSalva = solicitacaoRepo.save(solicitacao);

        //envia solicitacao para emissao
        filaEmissao.send(TOPICO_EMISSAO, "1", solicitacaoSalva);

        //envia solicitacao para log
        filaLog.send(TOPICO_LOG, "1", new LogNFe(solicitacaoSalva, "EMISSAO"));

        return solicitacaoSalva;
    }

    private void validaCPFouCNPJ(String identidade) {
        if (identidade.length() != 11 && identidade.length() != 14) throw new IdentidadeEmissaoNFeException(identidade);
    }

    public SolicitacaoNFe atualiza(SolicitacaoNFe atualizacao) {
        //salva NFe separadamente
        NFe nfe = atualizacao.getNfe();
        nfeRepo.save(nfe);
        //salva solicitacao com objeto NFe
        return solicitacaoRepo.save(atualizacao);
    }

    public List<SolicitacaoNFe> consulta(String identidade) {
        filaLog.send(TOPICO_LOG, "1", new LogNFe(identidade, "CONSULTA"));
        return solicitacaoRepo.findByIdentidade(identidade);
    }
}
