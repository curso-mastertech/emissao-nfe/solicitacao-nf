package br.com.mastertech.nfe.controller;

import br.com.mastertech.nfe.model.SolicitacaoNFe;
import br.com.mastertech.nfe.service.SolicitacaoNFeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/nfe")
public class SolicitacaoNFeController {

    @Autowired
    private SolicitacaoNFeService solicitacaoService;

    @PostMapping("/emitir")
    private SolicitacaoNFe solicita(@Valid @RequestBody SolicitacaoNFe solicitacaoNFe) {
        return solicitacaoService.solicita(solicitacaoNFe);
    }

    @PostMapping("/atualizar")
    private SolicitacaoNFe atualiza(@Valid @RequestBody SolicitacaoNFe atualizacaoNFe) {
        System.out.printf("Recebendo atualizacao de emissao com id: %s e cpf/cnpj: %s", atualizacaoNFe.getId(), atualizacaoNFe.getIdentidade());
        return solicitacaoService.atualiza(atualizacaoNFe);
    }

    @GetMapping("/consultar/{identidade}")
    private List<SolicitacaoNFe> consulta(@PathVariable String identidade) {
        return solicitacaoService.consulta(identidade);
    }
}
