package br.com.mastertech.nfe.model;

import java.time.LocalDateTime;

public class LogNFe {

    private String identidade;
    private String tipo;
    private String valor;
    private String timestamp;

    public LogNFe(SolicitacaoNFe solicitacaoNFe, String tipo) {
        this.identidade = solicitacaoNFe.getIdentidade();
        this.tipo = tipo;
        this.valor = solicitacaoNFe.getValor() + "";
        this.timestamp = LocalDateTime.now() + "";
    }

    public LogNFe(String identidade, String tipo) {
        this.identidade = identidade;
        this.tipo = tipo;
        this.timestamp = LocalDateTime.now() + "";
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
