package br.com.mastertech.nfe.repository;

import br.com.mastertech.nfe.model.NFe;
import br.com.mastertech.nfe.model.SolicitacaoNFe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NFeRepository extends CrudRepository<NFe, Long> {
}
