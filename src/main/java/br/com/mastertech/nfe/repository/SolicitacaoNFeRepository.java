package br.com.mastertech.nfe.repository;

import br.com.mastertech.nfe.model.SolicitacaoNFe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SolicitacaoNFeRepository extends CrudRepository<SolicitacaoNFe, Long> {
    List<SolicitacaoNFe> findByIdentidade(String identidade);
}
