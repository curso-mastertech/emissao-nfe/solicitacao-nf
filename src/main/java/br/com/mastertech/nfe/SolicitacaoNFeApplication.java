package br.com.mastertech.nfe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableFeignClients
public class SolicitacaoNFeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SolicitacaoNFeApplication.class, args);
    }

}
